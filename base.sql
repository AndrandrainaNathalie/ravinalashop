create database shopping;
use shopping;
create table categorie
(
	id int not null auto_increment primary key,
    nom varchar(100)
);

insert into categorie values ('','Homme');
insert into categorie values ('','Femme');
insert into categorie values ('','Enfant');
insert into categorie values ('','Bebe');

create table sousCategorie
(
	id int not null auto_increment primary key,
	idCategorie int,
    nom varchar(100)
);
insert into sousCategorie values ('',1,'pantalon');
insert into sousCategorie values ('',1,'Chemise');
insert into sousCategorie values ('',1,'Tee-shirt');
insert into sousCategorie values ('',1,'Casquete');
insert into sousCategorie values ('',1,'Chaussure');
insert into sousCategorie values ('',1,'Maillot de bain');
insert into sousCategorie values ('',1,'Short');

insert into sousCategorie values ('',2,'pantalon');
insert into sousCategorie values ('',2,'Robe');
insert into sousCategorie values ('',2,'Chaussure');
insert into sousCategorie values ('',2,'Tee-shirt');
insert into sousCategorie values ('',2,'Casquete');
insert into sousCategorie values ('',2,'Maillot de bain');
insert into sousCategorie values ('',2,'Accessoire');

insert into sousCategorie values ('',3,'pantalon');
insert into sousCategorie values ('',3,'Robe');
insert into sousCategorie values ('',3,'Tee-shirt');
insert into sousCategorie values ('',3,'Casquete');
insert into sousCategorie values ('',3,'Chaussure');
insert into sousCategorie values ('',3,'Maillot de bain');
insert into sousCategorie values ('',3,'Short');


insert into sousCategorie values ('',4,'pantalon');
insert into sousCategorie values ('',4,'Robe');
insert into sousCategorie values ('',4,'Tee-shirt');
insert into sousCategorie values ('',4,'Bonet');
insert into sousCategorie values ('',4,'Chaussure');
insert into sousCategorie values ('',4,'Ensemble');
insert into sousCategorie values ('',4,'Short');

create table produit(
	id int not null auto_increment primary key,
	idCategorie int,
	idSousCat int,
	nom varchar(50),
	prix double,
	disponible double,
	image blob,
	description varchar(100)
);
insert into produit values('',1, 1, 'Pantalon Jean pour Homme', 100, 50, '1.jpg','pantalon Jean pour les Hommes, des differentes tailles, commande disponible en ligne et livraison offert');
insert into produit values('',1, 1, 'Pantalon slim Homme', 150, 30, '2.jpg','pantalon slim pour les Hommes, des grandes disponible en ligne et livraison offert');
insert into produit values('',1, 1, 'Pantalon class Homme',200, 60, '3.jpg', 'pantalon class pour les Hommes, commande disponible en ligne et livraison offert');
insert into produit values('',1, 1, 'Pantalon Les chinos Homme', 300, 20, '4.jpg', 'pantalon Les chinos pour les Hommes, commande disponible en ligne et livraison offert');

insert into produit values('',1, 2, 'chemise gris Homme', 50, 120, '5.jpg','Chemise gris pour un homme avec un bouton noir, commande disponible en ligne et livraison offert');
insert into produit values('',1, 2, 'chemise blanc claire Homme', 80, 150, '6.jpg', 'Chemise pour un Homme blanc claire avec unn col V, commande disponible en ligne et livraison offert');
insert into produit values('',1, 2, 'chemise bleu rayé Homme', 60, 80, '7.jpg', 'Chemise Homme bleu rayé, commande disponible en ligne et livraison offert');
insert into produit values('',1, 2, 'chemise blanc-grise Homme', 45, 30, '8.jpg', 'Chemise blanc-grise vrai conton pour un Homme, commande disponible en ligne et livraison offert');

insert into produit values('',1, 3, 'Tee-shirt addidas Homme', 50, 120, '9.jpg','Tee-shirt addidas pour Homme diponible en toutes couleurs diponible, commande en ligne possible');
insert into produit values('',1, 3, 'Tee-shirt aubergine Homme', 80, 150, '10.jpg', 'Tee-shirt special aubergine pour un homme ideal, commande en ligne et livraison offert');
insert into produit values('',1, 3, 'Tee-shirt Levis Homme', 60, 80, '11.jpg','Tee-shirt blanc Levis specialement pour un Homme, commande en ligne et livraison offert');
insert into produit values('',1, 3, 'Tee-shirt noir Homme', 45, 30, '12.jpg','Tee-shirt noir avec un sqeulette de design specialement pour les Hommes, commande en ligne et livraison offert');

insert into produit values('',1, 4, 'Casquete beret Homme', 100, 50, '13.jpg', 'Casquete beret disponible en toutes couleurs, commande en ligne et livraison offert');
insert into produit values('',1, 4, 'Casquete noir Ambondona Homme', 150, 30, '14.jpg', 'Casquete noir marque Ambondona commande en ligne et livraison offert');
insert into produit values('',1, 4, 'Casquete noir Homme',200, 60, '15.jpg', 'Casquete noir avec un design dore des quantites sont disponible, commande en ligne et livraison offert');
insert into produit values('',1, 4, 'Casquete Homme', 300, 20, '16.jpg', 'Casquete bleu sous forme Casta de nouvel arrivage au Raviala Shop, commande en ligne et livraison offert');

insert into produit values('',1, 5, 'Chaussure Converse Homme', 100, 50, '17.jpg','Nouvel Arrivage de Converse pour un Homme avec un disign style et des grandes marques, commande en ligne et livraison offert');
insert into produit values('',1, 5, 'Chaussure Converse maron Homme', 150, 30, '18.jpg','Converse maron pour Homme, commande en ligne et livraison offert');
insert into produit values('',1, 5, 'Chaussure mocassin Homme',200, 60, '19.jpg', 'mocassin Homme, des differentes pointures, commande en ligne et livraison offert ');
insert into produit values('',1, 5, 'Chaussure Nike Homme', 300, 20, '20.jpg','Nike Huarache grande marque de Ravinala shop, des differentes pointures, commande en ligne et livraison offert');

insert into produit values('',1, 6, 'Maillot de bain Homme', 50, 120, '21.jpg','Maillot de bain Homme bleu avec un bande rouge sur le hanche, commande en ligne et livraison offert');
insert into produit values('',1, 6, 'Maillot de bain Homme', 80, 150, '22.jpg', 'Maillot de bain bleu ciel, vrai conton pour un Homme, commande en ligne et livraison offert');
insert into produit values('',1, 6, 'Maillot de bain Homme', 60, 80, '23.jpg', 'Maillot de bain bleu foncé pour Homme, resistible a l eau, commande en ligne et livraison offert');
insert into produit values('',1, 6, 'Maillot de bain Homme', 45, 30, '24.jpg', 'Maillot de bain bleu sous forme calson, elastique et souple, commande en ligne et livraison offert');

insert into produit values('',1, 7, 'Short vert armé Homme', 100, 50, '25.jpg', 'short vert armé specialement pour les jeunes Hommes, commande en ligne et livraison offert');
insert into produit values('',1, 7, 'Short Charoel Homme', 150, 30, '26.jpg','Short Charoel pour Homme avec un bande noire en bas, commande en ligne et livraison offert');
insert into produit values('',1, 7, 'Short jean Homme',200, 60, '27.jpg','Short jean bleu, vrai marque, commande en ligne et livraison offert');
insert into produit values('',1, 7, 'Short Vert armé foncé Homme', 300, 20, '28.jpg', 'Vert arme foncé, specialement pou un Hommme, commande en ligne et livraison offert');

insert into produit values('',2, 8, 'slim Jean', 100, 50, '29.jpg', 'Patalon slim jean noir pour le femme, elastique et souple, de differente taille, commande en ligne et livraison offert');
insert into produit values('',2, 8, 'slim destroy', 150, 30, '30.jpg', 'pantalon slim destroy jean pour le femme, differente couleur et taille au choix, commande en ligne et livraison offert');
insert into produit values('',2, 8, 'vert armé class',200, 60, '31.jpg', 'Patalon vert armé pour le femme, 4 poche, de differente taille, commande en ligne et livraison offert');
insert into produit values('',2, 8, 'Patalon Les chinos', 300, 200, '32.jpg', 'Patalon class rayé pour le femme, professionnel, de taille differente, commande en ligne et livraison offert');

insert into produit values('',2, 9, 'Robe fleur', 50, 120, '33.jpg', 'Robe de differente couleur, dos nu stylé de fleur, avec un seinture doré, commande en ligne et livraison offert');
insert into produit values('',2, 9, 'Robe format poison', 80, 150, '34.jpg', 'Robe format poison, de differente style, commande en ligne et livraison offert');
insert into produit values('',2, 9, 'Robe dentelle', 60, 80, '35.jpg','Robe dentelle, de couleur differente, plusieur taille disponible, commande en ligne et livraison offert');
insert into produit values('',2, 9, 'Robe en deux couleur', 45, 30, '36.jpg', 'Robe en deux couleur, selon le choix, dos partiellement nu en format noeud, commande en ligne et livraison offert');


insert into produit values('',2, 10, 'Chaussure talon caré noir Femme', 100, 50, '37.jpg', 'Chaussure talon caré noir, simple et leger, commande en ligne et livraison offert');
insert into produit values('',2, 10, 'Chaussure Nike Femme', 150, 30, '38.jpg', 'Chaussure Nike de vrai marque,  3 couleur disponible: noir-rouge- blanc, commande en ligne et livraison offert');
insert into produit values('',2, 10, 'Chaussure point fine Femme',200, 60, '39.jpg', 'Chaussure talon  point fine de 2cm, class et leger, commande en ligne et livraison offert');
insert into produit values('',2, 10, 'Chaussure air max Femme', 300, 20, '40.jpg', 'Chaussure air max, vrai marque, haute qualiité, de couleur au choix, commande en ligne et livraison offert');

insert into produit values('',2, 11, 'Tee-shirt fleurit Femme', 50, 120, '41.jpg'; 'Tee-shirt pour le femme rouge fleurit, de taille differente, commande en ligne et livraison offert');
insert into produit values('',2, 11, 'Tee-shirt class Femme', 80, 150, '42.jpg', 'Tee-shirt noir class  pour le femme, elastique, commande en ligne et livraison offert ');
insert into produit values('',2, 11, 'Tee-shirt blanc Femme', 60, 80, '43.jpg', 'Tee-shirt blanc, avec de design exeptionnel de couleur rose, commande en ligne et livraison offert');
insert into produit values('',2, 11, 'Tee-shirt conton Femme', 45, 30, '44.jpg', 'Tee-shirt conton vrai marque, de couleur differene, commande en ligne et livraison offert');

insert into produit values('',2, 12, 'Casquete beret Femme', 100, 50, '45.jpg', 'Casquete beret pour le femme, couleur, noir et aubergine, commande en ligne et livraison offert');
insert into produit values('',2, 12, 'Casquete beret armé  Femme', 150, 30, '46.jpg', 'Casquete beret armé specialement pour le femme, differente couleur, commande en ligne et livraison offert');
insert into produit values('',2, 12, 'Casquete noir Femme',200, 60, '47.jpg', 'Casquete noir, design rose, commande en ligne et livraison offert');
insert into produit values('',2, 12, 'Casquete noir cuir Femme', 300, 20, '48.jpg', 'Casquete noir cuir, haute qualite, commande en ligne et livraison offert');

insert into produit values('',2, 13, 'Maillot de bain Femme', 50, 120, '49.jpg', '');
insert into produit values('',2, 13, 'Maillot de bain Femme', 80, 150, '50.jpg');
insert into produit values('',2, 13, 'Maillot de bain Femme', 60, 80, '51.jpg');
insert into produit values('',2, 13, 'Maillot de bain Femme', 45, 30, '52.jpg');

insert into produit values('',2, 14, 'Accessoire colier', 100, 50, '53.png', 'Accesoir de colier, pendentif de diamant de differente couleur, ');
insert into produit values('',2, 14, 'Accessoire bague', 150, 30, '54.png', 'bague pour le femme en or, avec de diamant de couleur au choix, commande en ligne et livraison offert');
insert into produit values('',2, 14, 'Accessoire pendentif',200, 60, '55.png','pendentif en roulon, de diamant bruant, commande en ligne et livraison offert');
insert into produit values('',2, 14, 'Accessoire Montre', 300, 20, '56.png', 'Montre dore, avec un bague, commande en ligne et livraison offert');


insert into produit values('',3, 15, 'pantalon class Enfant',200, 60, '57.jpg', 'pantalon class pour enfant, des differentes tailles, commande disponible en ligne et livraison offert');
insert into produit values('',3, 15, 'pantalon Les chinos Enfant', 300, 200, '58.jpg', 'pantalon chinos pour les Hommes, des differentes tailles, commande disponible en ligne et livraison offert');
insert into produit values('',3, 15, 'pantalon Jean Enfant', 100, 50, '59.jpg', 'pantalon Jean pour les Hommes, des differentes tailles, commande disponible en ligne et livraison offert');
insert into produit values('',3, 15, 'pantalon slim Enfant', 150, 30, '60.jpg', 'pantalon slim pour les Hommes, des differentes tailles, commande disponible en ligne et livraison offert');

insert into produit values('',3, 16, 'Robe Enfant', 50, 120, '61.jpg', 'robe de differente couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');
insert into produit values('',3, 16, 'Robe de seinture gris Enfant', 80, 150, '62.jpg', 'robe de differente couleur de seinture gris, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');
insert into produit values('',3, 16, 'Robe en deux couleur Enfant', 60, 80, '63.jpg', 'robe en deux couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');
insert into produit values('',3, 16, 'Robe boucle Enfant', 45, 30, '64.jpg', 'robe boucle de differente couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');

insert into produit values('',3, 17, 'Tee-shirt Enfant', 50, 120, '65.jpg', 'Tee-shirt de differente couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');
insert into produit values('',3, 17, 'Tee-shirt Enfant', 80, 150, '66.jpg', 'Tee-shirt de differente couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');
insert into produit values('',3, 17, 'Tee-shirt Enfant', 60, 80, '67.jpg', 'Tee-shirt de differente couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');
insert into produit values('',3, 17, 'Tee-shirt Enfant', 45, 30, '68.jpg', 'Tee-shirt de differente couleur, pour enfant, disponible en plusieur taille, commande en ligne et livraison offert');

insert into produit values('',3, 18, 'Casquete Enfant', 100, 50, '71.jpg', 'Casquete de differente couleur, pour enfant, design format canard, commande en ligne et livraison offert');
insert into produit values('',3, 18, 'Casquete Enfant', 150, 30, '72.jpg', 'Casquete de differente couleur, pour enfant, environnement nuagé, commande en ligne et livraison offert');
insert into produit values('',3, 18, 'Casquete Nickel Enfant',200, 60, '69.jpg', 'Casquete Nickel de differente couleur, pour enfant, commande en ligne et livraison offert');
insert into produit values('',3, 18, 'Casquete Enfant', 300, 20, '70.jpg', 'Casquete vrai marque de differente couleur, pour enfant, commande en ligne et livraison offert');

insert into produit values('',3, 19, 'Chaussure de randoné Enfant', 100, 50, '75.jpg', 'Chaussure de randoné, pour enfant, differente couleur, commande en ligne et livraison offert');
insert into produit values('',3, 19, 'Chaussure Adidas Enfant', 150, 30, '76.jpg','Chaussure Adidas, pour enfant, commande en ligne et livraison offert');
insert into produit values('',3, 19, 'Chaussure timberland Enfant',200, 60, '73.jpg', 'Chaussure timberland neauvouté pour enfant, en ligne et livraison offert');
insert into produit values('',3, 19, 'Chaussure Balerine Enfant', 300, 20, '74.jpg', 'Balerine pour enfant, en ligne et livraison offert');

insert into produit values('',3, 20, 'Maillot de bain  Enfant', 50, 120, '79.jpg');
insert into produit values('',3, 20, 'Maillot de bain Enfant', 80, 150, '80.jpg');
insert into produit values('',3, 20, 'Maillot de bain Enfant', 60, 80, '77.jpg');
insert into produit values('',3, 20, 'Maillot de bain Enfant', 45, 30, '78.jpg');

insert into produit values('',3, 21, 'Short avec un noeud Enfant', 100, 50, '84.jpg', 'Short pour enfant, avec un noeud sur la taille, en ligne et livraison offert');
insert into produit values('',3, 21, 'Short elastique Enfant', 150, 30, '83.jpg', 'Short elastique et souple pour enfant, en ligne et livraison offert');
insert into produit values('',3, 21, 'Short roulon Enfant',200, 60, '81.jpg', 'Short avec un roulon des differentes couleurs, pour enfant, en ligne et livraison offert');
insert into produit values('',3, 21, 'Short rayé Enfant', 300, 20, '82.jpg', 'Short rayé pour enfant, en ligne et livraison offert');

insert into produit values('',4, 22, 'Patalon fleurit bebe', 100, 50, '88.jpg', 'Patalon fleurit pour bebe, chaud et leger, en ligne et livraison offert');
insert into produit values('',4, 22, 'Patalon rayé bebe', 150, 30, '87.jpg', 'Patalon rayé sur le hanche pour bebe, chaud et leger, en ligne et livraison offert');
insert into produit values('',4, 22, 'Patalon fleurit et rayé bebe',200, 60, '85.jpg', 'Patalon fleurit et rayé pour bebe, chaud et leger, en ligne et livraison offert');
insert into produit values('',4, 22, 'Patalon jean bebe', 300, 200, '86.jpg', 'Patalon jean pour bebe, chaud et leger, en ligne et livraison offert');

insert into produit values('',4, 23, 'Robe bebe fleurit', 50, 120, '92.jpg', 'Robe fleurit pour bebe, leger, en ligne et livraison offert');
insert into produit values('',4, 23, 'Robe dentelle bebe', 80, 150, '91.jpg', 'Robe dentelle pour bebe de differente couleur, leger, en ligne et livraison offert');
insert into produit values('',4, 23, 'Robe blanche bebe', 60, 80, '89.jpg', 'Robe blanche avec un seinture rose fleurit pour bebe, leger, en ligne et livraison offert');
insert into produit values('',4, 23, 'Robe rouge bebe', 45, 30, '90.jpg', 'Robe rouge pour bebe, avec  un design de rose blanc, leger et chaud, en ligne et livraison offert');

insert into produit values('',4, 24, 'Tee-shirt leger bebe', 50, 120, '96.jpg', 'Tee-shirt en differente couleur pour bebe, leger avec le photo de baby boss devant, en ligne et livraison offert');
insert into produit values('',4, 24, 'Tee-shirt leger bebe', 80, 150, '95.jpg', 'Tee-shirt en differente couleur pour bebe, leger, en ligne et livraison offert');
insert into produit values('',4, 24, 'Tee-shirt leger bebe', 60, 80, '93.jpg', 'Tee-shirt blanc  pour bebe, avec un design rose devant, leger, en ligne et livraison offert');
insert into produit values('',4, 24, 'Tee-shirt leger bebe', 45, 30, '94.jpg', 'Tee-shirt vert arme, pour bebe, leger, en ligne et livraison offert');

insert into produit values('',4, 25, 'Bonet chat blanc bebe', 100, 50, '100.jpg', 'Bonet bleu avec un chat blanc, pour bebe, en ligne et livraison offert');
insert into produit values('',4, 25, 'Bonet popone en haut bebe', 150, 30, '99.jpg', 'Bonet toute couleur avec un popone en haut, pour bebe, en ligne et livraison offer');
insert into produit values('',4, 25, 'Bonet grand popone  bebe',200, 60, '97.jpg', 'Bonet maron avec un grand popone , pour bebe, en ligne et livraison offer');
insert into produit values('',4, 25, 'Bonet popone chaté bebe', 300, 20, '98.jpg', 'Bonet toutes couleurs avec un popone chaté, pour bebe, en ligne et livraison offer');

insert into produit values('',4, 26, 'Chaussure botte bebe', 100, 50, '103.jpg', 'Chaussure botte noire pour bebe, en ligne et livraison offer');
insert into produit values('',4, 26, 'Chaussure converse  bebe', 150, 30, '104.jpg', 'Chaussure converse toutes couleurs pour bebe, en ligne et livraison offer');
insert into produit values('',4, 26, 'Chaussure Balerine bebe',200, 60, '101.jpg', 'Chaussure Balerine toutes couleurs pour bebe, en ligne et livraison offer');
insert into produit values('',4, 26, 'Chaussure Balerine maron bebe', 300, 20, '102.jpg', 'Chaussure Balerine maron avec un oiseau rose pour bebe, en ligne et livraison offer');

insert into produit values('',4, 27, 'Ensemble chemise et short', 50, 120, '107.jpg', 'Ensembre chemise blanc col claudine bleu et short class noir pour bebe, en ligne et livraison offer');
insert into produit values('',4, 27, 'Ensemble robe, chapeau et chaussure', 80, 150, '108.jpg', 'Ensemble de robe, chapeau et chaussure de meme couleur, pour bebe, en ligne et livraison offe');
insert into produit values('',4, 27, 'Ensemble greniere', 60, 80, '105.jpg', 'Ensemble greniere avec un coeur devant, en ligne et livraison offe');
insert into produit values('',4, 27, 'Ensemble greniere et un chapeau', 45, 30, '106.jpg', 'Ensemble greniere et un chapeau de meme couleur, en ligne et livraison offer');

insert into produit values('',4, 28, 'Short en belotte Bebe', 100, 50, '109.jpg', 'Short en belotte pour bebe, leger et chaud, en ligne et livraison offer');
insert into produit values('',4, 28, 'Short fleurit Bebe', 150, 30, '110.jpg', 'Short fleurit avec un seinture en noeud pour bebe, leger et chaud, en ligne et livraison offer');
insert into produit values('',4, 28, 'Short conton Bebe',200, 60, '111.jpg', 'Short en conton, leger mais chaud, en ligne et livraison offer');
insert into produit values('',4, 28, 'Short jean Bebe', 300, 20, '112.jpg', 'Short jean avec un seinture en noeud pour bebe, leger et chaud, en ligne et livraison offer');

create table admin(
id int not null auto_increment primary key,
nom varchar(100),
mdp varchar(100)
);

insert into admin values('', 'root', 'root');

	BackOffice:
	-table de données
	-upate base: image blog
	-crud
	-design pagination

	FrontOffice: 
	-alt et titre image dynamic
	-image blog
	-Test
	Rewriting BackOffice

	Heergement
	-webHost: site et base
	-code source sur github




