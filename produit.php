<?php
require_once('Fonction.php');
$base = new BASE();
$cat = $_GET['categorie'];
$sousCat = $_GET['sousCat'];
$pro = $base->getProduit($cat, $sousCat);
$nomCat =  $base->getCatById($cat);
$nomSousCat = $base->getSousCatById($sousCat);
?>
<!DOCTYPE html>
<html>
<head>
<title>Ravinalana Shop | <?php echo $nomSousCat[0][2] ?> <?php echo $nomCat[0][1] ?></title>
<meta name="Description" content="Ravinala Shop: vente de mode et de nouveauté de vetement en ligne, des divers produit et accesoires de femme sont disponible, shopping en toute categorie:Homme, Femme, enfant, specialent de " . <?php echo $nomSousCat[0][2] ?> <?php echo $nomCat[0][1]; ?>/>
<meta name="keywords" content="shop, ravinala, vetement, mode, panier, accessoire, en ligne, shopping, produit, vente, achat, acheter, " . <?php echo $nomSousCat[0][2]; ?> "," <?php echo $nomCat[0][1] ?>/>
<?php include('header.php'); ?>
</head>
<body>
<?php include('nav.php'); ?>
<div class="page-head">
	<div class="container">
		<h3><?php echo $nomSousCat[0][2] ?> sur Ravinalana shop</h3>
	</div>
</div>
<div class="electronics">
	<div class="container">
			<div class="ele-bottom-grid">
				<h1 align="center"><?php echo $nomSousCat[0][2] ?> <span><?php echo $nomCat[0][1] ?></span></h1>
				<p>Des produits de vrais marques sont disponibles en ligne sur <strong>Ravinala Shop</strong>, de nouveauté de <strong>vetement et de mode</strong> surtout de <strong><?php echo $nomSousCat[0][2] ?> <span><?php echo $nomCat[0][1] ?></strong></p>
				<?php for($i = 0; $i<sizeof($pro); $i++)
					{ ?>
					<div class="col-md-3 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									<img alt="<?php echo $pro[$i][7]; ?>" title="<?php echo $pro[$i][7]; ?>" src="data:image;base64,<?php echo $pro[$i][6]; ?>" class="pro-image-front">
									<img alt="<?php echo $pro[$i][7]; ?>" title="<?php echo $pro[$i][7]; ?>" src="data:image;base64,<?php echo $pro[$i][6]; ?>" class="pro-image-back">
										<div class="men-cart-pro">
											<div class="inner-men-cart-pro">
												<a href="<?php echo $pro[$i][3]."SHOP-"?><?php echo $pro[$i][0].".html"?>" class="link-product-add-cart">Voir</a>
											</div>
										</div>
										
								</div>
								<div class="item-info-product ">
									<h4><a href="<?php echo $pro[$i][3]."SHOP-"?><?php echo $pro[$i][0].".html"?>"><?php echo $pro[$i][3]?></a></h4>
									<div class="info-product-price">
										<span class="item_price">$<?php echo $pro[$i][4]?></span>
									</div>
									<a href="#" class="item_add single-item hvr-outline-out button2">Panier</a>									
								</div>
							</div>
						</div>

						<?php } ?>
						
						<div class="clearfix"></div>
			</div>
	</div>
</div>
<?php include('footer.php'); ?>
</body>
</html>