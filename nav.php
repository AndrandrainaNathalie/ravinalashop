<!-- header -->
<?php
require_once('Fonction.php');
$base = new BASE();
$c = $base->getCategorie();
?>
<div class="header">
	<div class="container">
		<ul>
			<li><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Livraison rapide</li>
			<li><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>achat simple et securisé</li>
			<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@example.com">ravinalashop@gmail.com</a></li>
		</ul>
	</div>
</div>
<!-- //header -->
<!-- header-bot -->
<div class="header-bot">
	<div class="container">
		<div class="col-md-3 header-left">
			<a href="index.html"><img src="images/ravinala-shop-logo.jpg" alt="logo de ravinala shop vente de vetement en ligne" title="logo de ravinala shop vente de vetement en ligne"></a>
		</div>
		<div class="col-md-6 header-middle">
			<form method="post" action="recherche.php">
				<div class="search">
					<input name= "nom" id="nom" type="search" value="recherche . . ." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
				</div>
				<div class="section_room">
					<select name = "cat" id= "cat" onchange="change_country(this.value)" class="frm-field required">
						<option value="0">Tous les categories</option>
					<?php  for($i = 0; $i<sizeof($c); $i++)
					{ ?>
						<option value="<?php echo $c[$i][0]?>"><?php echo $c[$i][1]?></option>
						<?php } ?>
					</select>
				</div>
				<div class="sear-sub">
					<input type="submit" value=" ">
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
		<div class="col-md-3 header-right footer-bottom">
			<ul>
				<li><a href="#" class="use1" data-toggle="modal" data-target="#myModal4"><span>Login</span></a>
					
				</li>
				<li><a class="fb" href="#"></a></li>
				<li><a class="twi" href="#"></a></li>
				<li><a class="insta" href="#"></a></li>
				<li><a class="you" href="#"></a></li>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //header-bot -->
<!-- banner -->
<div class="ban-top">
	<div class="container">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
				 
					<li class="active menu__item menu__item--current"><a class="menu__link" href="index.html">Acccueil <span class="sr-only">(current)</span></a></li>
					<?php  for($i = 0; $i<sizeof($c); $i++)
					{ ?>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $c[$i][1]?><span class="caret"></span></a>
							<ul class="dropdown-menu multi-column columns-3">
								<div class="row">
									<div class="col-sm-6 multi-gd-img1 multi-gd-text ">

										<?php $p = $base->produitRand($c[$i][0]); ?>

										<a href="<?php echo $p[0][3]."SHOP-"?><?php echo $p[0][0].".html"?>"><img src="data:image;base64,<?php echo $p[0][6]; ?>" alt="<?php echo $p[0][7]; ?>" title="<?php echo $p[0][7]; ?>" /></a>
									</div>
									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
										<?php $cc = $base->getSousCategorie($c[$i][0]); 
											for($j = 0; $j<sizeof($cc); $j++)
											{
										?>
											<li><a href="<?php echo $cc[$j][2]."RavinalaShop-".$c[$i][1]."-"?><?php echo $c[$i][0]."-" ?><?php echo $cc[$j][0].".html" ?>"><?php echo $cc[$j][2]?></a></li>
											<?php } ?>
										</ul>
									</div>
									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
					<?php } ?>
					<li class=" menu__item"><a class="menu__link" href="contact.php">Apropos</a></li>
					<li class=" menu__item"><a class="menu__link" href="contact.html">Contact</a></li>
				</div>
			  </div>
			</nav>	
		</div>
		<div class="top_nav_right">
			<div class="cart box_1">
						<a href="checkout.html">
							<h3> <div class="total">
								<i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
								<span class="simpleCart_total"></div>
								
							</h3>
						</a>
						<p><a href="javascript:;" class="simpleCart_empty">(1 Produits)</a></p>
						
			</div>		
			</div>
		<div class="clearfix"></div>
	</div>
</div>