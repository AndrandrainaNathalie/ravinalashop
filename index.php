<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once('Fonction.php');
$base = new BASE();
$ofr = $base->getOffre();
$nov = $base->getNouveau();
$col = $base->getCollection();
?>
<!DOCTYPE html>
<html>
<head>
<title>Ravinalana Shop | Vente de vetement en ligne</title>
<meta name="Description" content="Ravinala Shop: vente de mode et de nouveauté de vetement en ligne, des divers produit et accesoires sont disponible, shopping en toute categorie:Homme, Femme, enfant, specialent des pentalon, chemise, robe, colier, short, tee-Shirt, casquette, chaussure, Maillot de bain"/>
<meta name="keywords" content="shop, ravinala, vetement, mode, accessoire, panier, en ligne, shopping, produit, vente, achat, acheter, pentalon, chemise, robe, colier, short, tee-Shirt, casquette, chaussure, Maillot de bain"/>
<?php include('header.php'); ?>
</head>
<body>
<?php include('nav.php'); ?>
<!-- //banner-top -->
<!-- banner -->
<div class="banner-grid">
	<div id="visual">
			<div class="slide-visual">
				<!-- Slide Image Area (1000 x 424) -->
				<ul class="slide-group">
					<li><img class="img-responsive" src="images/ravinala-shop-vetement-slider1.jpg" alt="vetement en ligne de ravinala shop slider" title="vetement en ligne de ravinala shop slider" /></li>
					<li><img class="img-responsive" src="images/ravinala-shop-vetement-slider2.jpg" alt="vetement en ligne de ravinala shop slider" title="vetement en ligne de ravinala shop slider" /></li>
					<li><img class="img-responsive" src="images/ravinala-shop-vetement-slider3.jpg" alt="vetement en ligne de ravinala shop slider" title="vetement en ligne de ravinala shop slider" /></li>
				</ul>

				<!-- Slide Description Image Area (316 x 328) -->
				<div class="script-wrap">
					<ul class="script-group">
						<li><div class="inner-script"><img class="img-responsive" src="images/ravinala-shop-vetement-slid1.jpg" alt="vetement en ligne de ravinala shop slider" title="vetement en ligne de ravinala shop slider" /></div></li>
						<li><div class="inner-script"><img class="img-responsive" src="images/ravinala-shop-vetement-slid2.jpg" alt="vetement en ligne de ravinala shop slider" title="vetement en ligne de ravinala shop slider" /></div></li>
						<li><div class="inner-script"><img class="img-responsive" src="images/ravinala-shop-vetement-slid3.jpg" alt="vetement en ligne de ravinala shop slider" title="vetement en ligne de ravinala shop slider" /></div></li>
					</ul>
					<div class="slide-controller">
						<a href="#" class="btn-prev"><img src="images/btn_prev.png" alt="Prev Slide" /></a>
						<a href="#" class="btn-play"><img src="images/btn_play.png" alt="Start Slide" /></a>
						<a href="#" class="btn-pause"><img src="images/btn_pause.png" alt="Pause Slide" /></a>
						<a href="#" class="btn-next"><img src="images/btn_next.png" alt="Next Slide" /></a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	<script type="text/javascript" src="js/pignose.layerslider.js"></script>
	<script type="text/javascript">
	//<![CDATA[
		$(window).load(function() {
			$('#visual').pignoseLayerSlider({
				play    : '.btn-play',
				pause   : '.btn-pause',
				next    : '.btn-next',
				prev    : '.btn-prev'
			});
		});
	//]]>
	</script>

</div>
<!-- //banner -->
<!-- content -->

<div class="new_arrivals">
	<div class="container">
	<h1 align="center">Ravinalana Shop</h1>
	<br>
		<h3><span>Ravinala </span>top produit</h3>
		<p>Le meilleur choix de shopping en ligne à Madagascar</p>
		<div class="new_grids">
			<div class="col-md-4 new-gd-left">
				<img src="images/ravinala-shop-top-produit-vetement-marriage.jpg" alt="ravinala shop vetement special marriage top produit" title="ravinala shop vetement special marriage top produit" />
				<div class="wed-brand simpleCart_shelfItem">
					<h4>Ravinala Collection pour le marriage</h4>
					<h5>prix abordable</h5>
					<p> <span class=""> $250 à $500</span><a class="item_add hvr-outline-out button2" href="#">Detail </a></p>
				</div>
			</div>
			<div class="col-md-4 new-gd-middle">
				<div class="new-levis">
					<div class="mid-img">
						<img src="images/mvola.jpg" alt=" " />
					</div>
					<div class="mid-text">
						<h4>5% de Reduction</h4>
						<a class="hvr-outline-out button2" href="product.php">Shopping</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="new-levis">
					<div class="mid-text">
						<h4>2% de Reduction</h4>
						<a class="hvr-outline-out button2" href="product.php">Shopping</a>
					</div>
					<div class="mid-img">
						<img src="images/paypal.jpg" alt=" " />
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-4 new-gd-left">
				<a href="homme.php"><img src="images/ravinala-shop-top-produit-vetement-homme.jpg" alt="ravinala shop vetement special marriage top produit pour homme" title="ravinala shop vetement special marriage top produit pour homme" /></a>
				<div class="wed-brandtwo simpleCart_shelfItem">
					<h4>Ravinala Collections</h4>
					<p>Homme</p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //content -->

<!-- content-bottom -->

<div class="content-bottom">
	<div class="col-md-7 content-lgrid">
		<div class="col-sm-6 content-img-left text-center">
			<div class="content-grid-effect slow-zoom vertical">
				<div class="img-box"><img src="images/ravinala-shop-accessoire-bracelier.png" alt="Ravinala shop special Accesoire bracelier pour femme" title="Ravinala shop special Accesoire bracelier pour femme" class="img-responsive zoom-img"></div>
					<div class="info-box">
						<div class="info-content simpleCart_shelfItem">
									<h4>Brecelier</h4>
									<span class="separator"></span>
									<p><span class="item_price">$<?php echo 500 - 500*20/100?></span></p>
									<p><del>$500</del></p>
									<span class="separator"></span>
									<a class="item_add hvr-outline-out button2" href="#">Panier</a>
						</div>
					</div>
			</div>
		</div>
		<div class="col-sm-6 content-img-right">
			<h3>Offre special<span>Chaussures</span>Reduction de 20%</h3>
		</div>
		
		<div class="col-sm-6 content-img-right">
			<h3>Acheter un et 1 offert par <strong>Ravinala</strong><span> Accessoires</span> Nouveauté</h3>
		</div>
		<div class="col-sm-6 content-img-left text-center">
			<div class="content-grid-effect slow-zoom vertical">
				<div class="img-box"><img src="images/ravinala-shop-accessoire-montre.jpg" alt="Ravinala shop special Accesoire montre standard" title="Ravinala shop special Accesoire montre standard" class="img-responsive zoom-img"></div>
					<div class="info-box">
						<div class="info-content simpleCart_shelfItem">
							<h4>Montre</h4>
							<span class="separator"></span>
							<p><span class="item_price">$<?php echo 250 - 250*20/100?></span></p>
							<p><del>$250</del></p>
							<span class="separator"></span>
							<a class="item_add hvr-outline-out button2" href="#">Ajouer</a>
						</div>
					</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-5 content-rgrid text-center">
		<div class="content-grid-effect slow-zoom vertical">
				<div class="img-box"><img src="images/ravinala-shop-chaussure-converse.jpg" alt="Ravinala shop de chaussure converse toute categorie" title="Ravinala shop de chaussure converse toute categorie" class="img-responsive zoom-img"></div>
					<div class="info-box">
						<div class="info-content simpleCart_shelfItem">
									<h4>Converse</h4>
									<span class="separator"></span>
									<p><span class="item_price">$<?php echo 150 - 150*20/100?></span></p>
									<p><del>$150</del></p>
									<span class="separator"></span>
									<a class="item_add hvr-outline-out button2" href="#">Panier</a>
						</div>
					</div>
			</div>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //content-bottom -->
<!-- product-nav -->

<div class="product-easy">
	<div class="container">
		
		<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
		<script type="text/javascript">
							$(document).ready(function () {
								$('#horizontalTab').easyResponsiveTabs({
									type: 'default', //Types: default, vertical, accordion           
									width: 'auto', //auto or any width like 600px
									fit: true   // 100% fit in a container
								});
							});
							
		</script>
		<div class="sap_tabs">
			<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
				<ul class="resp-tabs-list">
					<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Dernier arrivage de veteement</span></li> 
					<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Offre special</span></li> 
					<li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>Collections</span></li> 
				</ul>				  	 
				<div class="resp-tabs-container">
					<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
					<?php for($i = 0; $i<sizeof($nov); $i++)
					{ ?>

						<div class="col-md-3 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									<img alt="<?php echo $nov[$i][7]; ?>" title="<?php echo $nov[$i][7]; ?>" src="data:image;base64,<?php echo $nov[$i][6]; ?>" class="pro-image-front">
									<img alt="<?php echo $nov[$i][7]; ?>" title="<?php echo $nov[$i][7]; ?>" src="data:image;base64,<?php echo $nov[$i][6]; ?>" class="pro-image-back">
										<div class="men-cart-pro">
											<div class="inner-men-cart-pro">
												<a href="<?php echo $nov[$i][3]."SHOP-"?><?php echo $nov[$i][0].".html"?>" class="link-product-add-cart">Voir</a>
											</div>
										</div>
										<span class="product-new-top">nouveauté</span>
										
								</div>
								<div class="item-info-product ">
									<h4><a href="<?php echo $nov[$i][3]."SHOP-"?><?php echo $nov[$i][0].".html"?>"><?php echo $nov[$i][3]?></a></h4>
									<div class="info-product-price">
										<span class="item_price">$<?php echo $nov[$i][4]?></span>
									</div>
									<a href="#" class="item_add single-item hvr-outline-out button2">Panier</a>									
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="clearfix"></div>
					</div>

					<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
					<?php for($i = 0; $i<sizeof($ofr); $i++)
					{ ?>

						<div class="col-md-3 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									<img alt="<?php echo $ofr[$i][7]; ?>" title="<?php echo $ofr[$i][7]; ?>" src="data:image;base64,<?php echo $ofr[$i][6]; ?>"  class="pro-image-front">
									<img alt="<?php echo $ofr[$i][7]; ?>" title="<?php echo $ofr[$i][7]; ?>" src="data:image;base64,<?php echo $ofr[$i][6]; ?>"  class="pro-image-back">
										<div class="men-cart-pro">
											<div class="inner-men-cart-pro">
												<a href="<?php echo $ofr[$i][3]."SHOP-"?><?php echo $ofr[$i][0].".html"?>" class="link-product-add-cart">Voir</a>
											</div>
										</div>
										
								</div>
								<div class="item-info-product ">
									<h4><a href="<?php echo $ofr[$i][3]."SHOP-"?><?php echo $ofr[$i][0].".html"?>"><?php echo $ofr[$i][3]?></a></h4>
									<div class="info-product-price">
										<span class="item_price">$<?php echo $ofr[$i][4] - $ofr[$i][4] * 20/100 ?></span>
										<del>$<?php echo $ofr[$i][4]?></del>
									</div>
									<a href="#" class="item_add single-item hvr-outline-out button2">Panier</a>									
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="clearfix"></div>						
					</div>

					<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
						<?php for($i = 0; $i<sizeof($col); $i++)
					{ ?>
						<div class="col-md-3 product-men yes-marg">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									<img alt="<?php echo $col[$i][7]; ?>" title="<?php echo $col[$i][7]; ?>" src="data:image;base64,<?php echo $col[$i][6]; ?>"  class="pro-image-front">
									<img alt="<?php echo $col[$i][7]; ?>" title="<?php echo $col[$i][7]; ?>" src="data:image;base64,<?php echo $col[$i][6]; ?>"  class="pro-image-back">
										<div class="men-cart-pro">
											<div class="inner-men-cart-pro">
												<a href="<?php echo $col[$i][3]."SHOP-"?><?php echo $col[$i][0].".html"?>">Voir</a>
											</div>
										</div>
										
								</div>
								<div class="item-info-product ">
									<h4><a href="<?php echo $col[$i][3]."SHOP-"?><?php echo $col[$i][0].".html"?>"><?php echo $col[$i][3]?></a></h4>
									<div class="info-product-price">
										<span class="item_price">$<?php echo $col[$i][4]?></span>
									</div>
									<a href="#" class="item_add single-item hvr-outline-out button2">Panier</a>									
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="clearfix"></div>		
					</div>	
				</div>	
			</div>
		</div>
	</div>
</div>
<!-- //product-nav -->

<?php include('footer.php'); ?>
<!-- //login -->
</body>
</html>
