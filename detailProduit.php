<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once('Fonction.php');
$base = new BASE();
$id = $_GET['id'];
$pid = $base->getProduiById($id);
$idc = $pid[0][2];
$cid = $base->getSousCatById($idc);
?>
<!DOCTYPE html>
<html>
<head>
<title>Ravinalana Shop | <?php echo $pid[0][3] ?></title>
<meta name="Description" content="Ravinala Shop: vente de mode et de nouveauté de vetement en ligne, des divers produit et accesoires de femme sont disponible, shopping en toute categorie:Homme, Femme, enfant, specialent de " . <?php echo $pid[0][3]; ?>/>
<meta name="keywords" content="shop, ravinala, vetement, mode, panier, accessoire, en ligne, shopping, produit, vente, achat, acheter, " . <?php echo $pid[0][3]; ?>. "," . <?php echo $cid[0][2]; ?>/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- single -->
<script src="js/imagezoom.js"></script>
<script src="js/jquery.flexslider.js"></script>
<!-- single -->
<!-- cart -->
	<script src="js/simpleCart.min.js"></script>
<!-- cart -->
<!-- for bootstrap working -->
	<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- //for bootstrap working -->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
<script src="js/jquery.easing.min.js"></script>
</head>
<body>
<?php include('nav.php'); ?>
<div class="page-head">
	<div class="container">
		<h3>produit Ravinala Shop</h3>
	</div>
</div>
<!-- //banner -->
<!-- single -->
<div class="single">
	<div class="container">
		<div class="col-md-6 single-right-left animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
			<div class="grid images_3_of_2">
				<div class="flexslider">
					<!-- FlexSlider -->
						<script>
						// Can also be used with $(document).ready()
							$(window).load(function() {
								$('.flexslider').flexslider({
								animation: "slide",
								controlNav: "thumbnails"
								});
							});
						</script>
					<!-- //FlexSlider-->
					<ul class="slides">
						<li data-thumb="images/d2.jpg">
							<div class="thumb-image"> <img alt="<?php echo $pid[0][7]; ?>" title="<?php echo $pid[0][7]; ?>" src="data:image;base64,<?php echo $pid[0][6]; ?>"  data-imagezoom="true" class="img-responsive"> </div>
						</li>	
					</ul>
					<div class="clearfix"></div>
				</div>	
			</div>
		</div>
		<div class="col-md-6 single-right-left simpleCart_shelfItem animated wow slideInRight animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInRight;">
					<h1><?php echo $pid[0][3]?></h1>
					<br>
					<h4><span class="item_price">Prix: $<?php echo  $pid[0][4] ?></span></h4>
					<br>
					<h4><span class="item_price">disponible: <?php echo $pid[0][5] ?></span></h4>
					</br>
					<div class="color-quality">
						<div class="color-quality-right">
							<select id="country1" onchange="change_country(this.value)" class="frm-field required sect">
								<option value="null">5 Qty</option>
								<option value="null">6 Qty</option> 
								<option value="null">7 Qty</option>					
								<option value="null">10 Qty</option>								
							</select>
						</div>
					</div>
					</br>
					<h4>Description:</h4>
					<br>
					<h5><?php echo $pid[0][7] ?></h5>
					</br>
					<div class="occasion-cart">
						<a href="#" class="item_add hvr-outline-out button2">Panier</a>
					</div>
					
		</div>
				<div class="clearfix"> </div>
	</div>
</div>

<?php include('footer.php'); ?>
<!-- //login -->
</body>
</html>
