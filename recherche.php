<?php
require_once('Fonction.php');
$base = new BASE();
$cat = $_POST['cat'];
$nom = $_POST['nom'];
$rech = $base->getRecherche($nom, $cat);
?>
<!DOCTYPE html>
<html>
<?php include('header.php'); ?>
<body>
<?php include('nav.php'); ?>
<div class="page-head">
	<div class="container">
		<h3>Ravinala produit</h3>
	</div>
</div>
<div class="electronics">
	<div class="container">
			<div class="ele-bottom-grid">
				<h3>Resultat de recherche</span></h3>
				<p>Des produits de vrais marques sont disponibles en ligne sur Ravinala Shop, de nouveauté de mode Internationnal</p>
		<?php for($i = 0; $i<sizeof($rech); $i++)
					{ ?>

						<div class="col-md-3 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									<img src="images/ph4.png" alt="" class="pro-image-front">
									<img src="images/ph4.png" alt="" class="pro-image-back">
										<div class="men-cart-pro">
											<div class="inner-men-cart-pro">
												<a href="single.html" class="link-product-add-cart">Voir</a>
											</div>
										</div>
										
								</div>
								<div class="item-info-product ">
									<h4><a href="single.html"><?php echo $rech[$i][3]?></a></h4>
									<div class="info-product-price">
										<span class="item_price"><?php echo $rech[$i][4]?></span>
									</div>
									<a href="#" class="item_add single-item hvr-outline-out button2">Panier</a>									
								</div>
							</div>
						</div>
						<?php 
						} ?>
						
						<div class="clearfix"></div>
			</div>
	</div>
</div>
<?php include('footer.php'); ?>
</body>
</html>