<?php

require_once('Connexion.php');

class BASE {

    private $conn;

    function __construct() {
        $database = new Connexion();
        $db = $database->dbConnection();
        $this->conn = $db;
    }

    public function runQuery($sql) {
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    public function getCategorie() {
        try {
            $stmt = $this->conn->query("SELECT * FROM categorie");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
   
    public function getSousCategories() {
        try {
            $stmt = $this->conn->query("SELECT * FROM souscategorie");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
    public function getSousCategorie($categorie) {
        try {
            $stmt = $this->conn->query("SELECT * FROM souscategorie where idCategorie =". $categorie ."");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
    public function produitRand($categorie) {
        try {
            $stmt = $this->conn->query("SELECT * FROM produit where idCategorie =". $categorie ." order by rand() limit 1");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
    public function getOffre() {
        try {
            $stmt = $this->conn->query("SELECT * FROM produit order by rand() limit 4");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
    public function getNouveau() {
        try {
            $stmt = $this->conn->query("SELECT * FROM produit order by id desc limit 4");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
        public function getCollection() {
        try {
            $stmt = $this->conn->query("SELECT * FROM produit order by rand() limit 4");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
    public function getProduit($cat, $sousCat) {
        try {
            $stmt = $this->conn->query("SELECT * FROM produit where idCategorie = '". $cat ."' and idSousCat = '". $sousCat ."'");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
   public function getCatById($id) {
        try {
            $stmt = $this->conn->query("SELECT * FROM categorie where id = ". $id ."");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
     public function getSousCatById($id) {
        try {
            $stmt = $this->conn->query("SELECT * FROM souscategorie where id = ". $id);
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
      public function getRecherche($nom, $cat) {
        try {
            if($cat == 0)
            {
                 $stmt = $this->conn->query("SELECT * FROM produit where nom  like lower('%". $nom ."%')");
            }
            else
            {
                 $stmt = $this->conn->query("SELECT * FROM produit where nom  like lower('%". $nom ."%') and idCategorie = '". $cat ."'");
            }
           
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
       public function getProduiById($id) {
        try {
            $stmt = $this->conn->query("SELECT * FROM produit where id = ". $id ."");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
     public function  getConnexionAdmin($nom,$mdp){
        try {
            $stmt = $this->conn->query("select*from admin where nom = '" .$nom. "' and mdp = '" .$mdp. "'");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            throw $e;
        }
    }

      public function produit() {
        try {
            $stmt = $this->conn->query("select*from produit p join categorie c on p.idCategorie = c.id join souscategorie s on p.idSousCat = s.id");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
        public function getCategorieNom() {
        try {
            $stmt = $this->conn->query("SELECT s.id, c.nom, s.nom FROM categorie c join  souscategorie s on c.id = s.idCategorie");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }
        function updateSousCat($id, $nom, $idCategorie) {
        $stmt = $this->conn->prepare("update souscategorie set nom='". $nom ."' and idCategorie= '". $idCategorie ."' where id='" . $id . "'");
        $stmt->bindParam('".$id."', $id);
        $stmt->bindParam('".$nom."', $nom);
        $stmt->bindParam('".$idCategorie."', $idCategorie);
        $stmt->execute();
        return true;
    }

    function deleteSousCat($id) {
        $stmt = $this->conn->prepare("delete from souscategorie where id ='" . $id . "'");
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        return true;
    }

    public function insertSousCategorie($idCategorie, $nom) {
        $stmt = $this->conn->prepare("INSERT INTO souscategorie(idCategorie,nom) VALUES(" . $idCategorie . ",'" . $nom . "')");
        $stmt->bindParam('".$idCategorie."', $idCategorie);
        $stmt->bindParam('".$nom."', $nom);

        $stmt->execute();
        return true;
    }
    public function getCategorieNomById($id) {
        try {
            $stmt = $this->conn->query("SELECT s.id, c.nom, s.nom FROM categorie c join  souscategorie s on c.id = s.idCategorie where s.id=". $id ."");
            $temp = array();
            $i = 0;
            while ($donnees = $stmt->fetch()) {
                $temp[$i] = $donnees;
                $i++;
            }

            return $temp;
        } catch (Exception $e) {
            
        }

    }

    public function insertProduit($idCategorie, $idSousCat, $nom, $prix, $disponible, $image, $description) {
        $stmt = $this->conn->prepare("INSERT INTO produit(idCategorie,idSousCat, nom, prix, disponible, image, description) VALUES(" . $idCategorie . "," . $idSousCat . ",'" . $nom . "',". $prix .", " . $disponible . ", '" . $image . "', '" . $description . "')");
        $stmt->bindParam('".$idCategorie."', $idCategorie);
        $stmt->bindParam('".$idSousCat."', $idSousCat);
        $stmt->bindParam('".$nom."', $nom);
        $stmt->bindParam('".$prix."', $prix);
        $stmt->bindParam('".$disponible."', $disponible);
        $stmt->bindParam('".$description."', $description);

        $stmt->execute();
        return true;
    }
    public function updateProduit($id, $idCategorie, $idSousCat, $nom, $prix, $disponible, $image, $description) {
        $stmt = $this->conn->prepare("update produit set idCategorie=" . $idCategorie . ", idSousCat=" . $idSousCat . ",nom='" . $nom . "',prix=". $prix .",disponible = " . $disponible . ", image = '" . $image . "', description = '" . $description . "' where id= '" . $id . "'  ");
        $stmt->bindParam('".$id."', $id);
        $stmt->bindParam('".$idCategorie."', $idCategorie);
        $stmt->bindParam('".$idSousCat."', $idSousCat);
        $stmt->bindParam('".$nom."', $nom);
        $stmt->bindParam('".$prix."', $prix);
        $stmt->bindParam('".$disponible."', $disponible);
        $stmt->bindParam('".$description."', $description);

        $stmt->execute();
        return true;
    }
     function deleteProduit($id) {
        $stmt = $this->conn->prepare("delete from produit where id ='" . $id . "'");
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        return true;
    }
}