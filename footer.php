<?php
require_once('Fonction.php');
$base = new BASE();
$c = $base->getCategorie();
?>
<div class="coupons">
	<div class="container">
		<div class="coupons-grids text-center">
			<div class="col-md-3 coupons-gd">
				<h3>acheter votre <strong>vetement sur Ravinala shop</strong></h3>
			</div>
			<div class="col-md-3 coupons-gd">
				<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
				<h4>Connecter à un compte sur <strong>Ravinala</strong></h4>
				<p>Pour faire un commande, il suffit de ouvrir votre compte. L'inscription est aussi gratuit.</p>
			</div>
			<div class="col-md-3 coupons-gd">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<h4>Choisir vos produits de marque Ravinala</h4>
				<p>De produit de marque et de neauvouté sont disponibles sur <strong>Ravinala shop</strong>, faite votre commande gratuitement.</p>
			</div>
			<div class="col-md-3 coupons-gd">
				<span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
				<h4>Ravinala shop: Payement securisé</h4>
				<p>Payement securisé: sur mobil money: Mvola, orange money, airtel Money; sur un virement paypal.</p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- footer -->
<div class="footer">
	<div class="container">
		<div class="col-md-3 footer-left">
			<h2><a href="index.html"><img src="images/ravinala-shop-logo.jpg" alt="logo de ravinala shop vente de vetement en ligne" title="logo de ravinala shop vente de vetement en ligne" /></a></h2>
			<h4>Ravinala Device</h4>
			<p>Toujours à votre service,</p>
			<p> prete à ofrir le meilleur produit pour vous satisfaire,</p>
			<p> avec des promotions à profiter chaque jours .</p>
		</div>
		<div class="col-md-9 footer-right">
			<div class="col-sm-6 newsleft">
				<h3>SUIVRE LES NOUVEAUTES</h3>
			</div>
			<div class="col-sm-6 newsright">
				<form>
					<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="submit" value="Submit">
				</form>
			</div>
			<div class="clearfix"></div>
			<div class="sign-grds">
				<div class="col-md-4 sign-gd">
					<h4>Information</h4>
					<ul>	
					<?php  for($i = 0; $i<sizeof($c); $i++)
					{ ?>
						<li><a href="index.html"><?php echo $c[$i][1]?></a></li>
						<?php } ?>
						<li><a href="apropos.php">Apropos</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</div>
				
				<div class="col-md-4 sign-gd-two">
					<h4>Magasin</h4>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Analakely lot IBM 448, <span>Antannarivo.</span></li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:ravinalashop@gmail.com">ravialashop@gmail.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +1234 567 567</li>
					</ul>
				</div>
				<div class="col-md-4 sign-gd flickr-post">
					<h4>Nous suivre</h4>
					<div class="col-md-4 header-right footer-bottom">
				<ul><li><a class="fb" href="#"></a></li></ul>
				<ul><li><a class="twi" href="#"></a></li></ul>	
				<ul><li><a class="you" href="#"></a></li></ul>
					
				</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
		<p class="copy-right">&copy 2018 IT-University | par <a href="realisatrice.php">Nathalie ANDRANDRAINA 49A (ETU00544)</a></p>
		<p align="center"><a href="RavinalaBackOffice/">Administration</a></span></p>
	</div>
</div>
<!-- //footer -->
<!-- login -->
			<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
							<div class="login-grids">
								<div class="login">
									<div class="login-bottom">
										<h3>Créer un compte</h3>
										<form>
											<div class="sign-up">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
											</div>
											<div class="sign-up">
												<h4>Mot de passe :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												
											</div>
											<div class="sign-up">
												<h4>Confirmation mot de passe :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												
											</div>
											<div class="sign-up">
												<input type="submit" value="INSCRIPTION" >
											</div>
											
										</form>
									</div>
									<div class="login-right">
										<h3>Déjà membre</h3>
										<form>
											<div class="sign-in">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
											</div>
											<div class="sign-in">
												<h4>mot de passe :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												<a href="profil.php">mot de passe oublié?</a>
											</div>
											<div class="single-bottom">
												<input type="checkbox"  id="brand" value="">
												<label for="brand"><span></span>Enregister le mot de passe.</label>
											</div>
											<div class="sign-in">
												<input type="submit" value="CONNEXION" >
											</div>
										</form>
									</div>
									<div class="clearfix"></div>
								</div>
								<p>Vous acceptez <a href="apropos.php">les termes et coditions</a> utilisés pour ce site</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>